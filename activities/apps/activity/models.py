# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib.postgres.fields import JSONField
from django.db import models
import datetime

# Create your models


class Property(models.Model):
    """
    Modelo para almacenar las propiedades de actividades
    """
    STATUS_CHOICES = (
        ('active', 'Activa'),
        ('disable', 'Desactivada'),
    )
    
    title = models.CharField(max_length=255, verbose_name=_("Título"))
    address = models.TextField(verbose_name=_("Dirección"))
    description = models.TextField(verbose_name=_("Descripción"))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Fecha y hora de creación"))
    updated_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Fecha y hora  de actualización"))
    disabled_at = models.DateTimeField(verbose_name=_("Fecha y hora en que se desactivo"), null=True, blank=True)
    status = models.CharField(max_length=35, choices=STATUS_CHOICES, default='active')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _("Propiedad")
        verbose_name_plural = _("Propiedades")

    def save(self, *args, **kwargs):
        self.updated_at = datetime.datetime.now()
        return super(Property, self).save(*args, **kwargs)


class Activity(models.Model):
    """
    Modelo para almacenar las actividades
    """
    STATUS_CHOICES = (
        ('active', 'Activa'),
        ('done', 'Realizada'),
        ('cancelled', 'Cancelada'),
    )
    property = models.ForeignKey(Property, on_delete=models.CASCADE, verbose_name=_("Propiedad"))
    schedule = models.DateTimeField(verbose_name=_("Fecha agendada"))
    title = models.CharField(max_length=255, verbose_name=_("Título"))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Fecha de creación"))
    updated_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Fecha de actualización"))
    status = models.CharField(max_length=35, choices=STATUS_CHOICES, default='active')

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.updated_at = datetime.datetime.now()
        return super(Activity, self).save(*args, **kwargs)

    def get_condition(self):
        condition = ""
        if self.status == "active":
            if self.schedule.date() >= datetime.datetime.now().date():
                condition = "Pendiente a realizar"
            else:
                condition = "Atrasada"
        if self.status == "done":
            condition = "Finalizada"
        return condition

    class Meta:
        verbose_name = _("Actividad")
        verbose_name_plural = _("Actividades")


class Survey(models.Model):
    """
    Modelo para almacenar las encuestas 
    """
    activity = models.ForeignKey(Activity, on_delete=models.CASCADE, verbose_name=_("Actividad"))
    answers = JSONField(verbose_name=_("Respuestas"))
    created_at = models.DateTimeField(auto_now_add=True, verbose_name=_("Fecha de creación"))

    def __str__(self):
        return self.activity.title

    class Meta:
        verbose_name = _("Encuesta")
        verbose_name_plural = _("Encuestas")