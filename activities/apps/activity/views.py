# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.http import HttpResponse

from activities.apps.activity.filters import ActivityFilter
from activities.apps.activity.models import Activity, Property
from activities.apps.activity.serializers import ActivitySerializer, PropertySerializer, RescheduleActivitySerializer
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.decorators import action
from datetime import datetime, timedelta

# Create your views here.


class ActivityViewSet(viewsets.ModelViewSet):
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer
    filterset_class = ActivityFilter

    def get_serializer_class(self):
        if self.action == 'reschedule_activity':
            return RescheduleActivitySerializer
        return ActivitySerializer

    def get_queryset(self):
        start_date = self.request.query_params.get('start_date', None)
        end_date = self.request.query_params.get('end_date', None)
        status = self.request.query_params.get('status', None)
        queryset = self.queryset
        exist_filter = False

        if start_date and end_date:
            start_date = datetime.strptime(start_date, '%Y-%m-%d')
            end_date = datetime.strptime(end_date, '%Y-%m-%d')
            start_filter = start_date.date()
            end_filter = end_date.date() + timedelta(days=1)
            queryset = self.queryset.filter(schedule__range=(start_filter, end_filter))
            exist_filter = True
        if status:
            queryset = queryset.filter(status=status)
            exist_filter
        if not exist_filter:
            start_filter = datetime.now() + timedelta(days=-3)
            end_filter = datetime.now() + timedelta(weeks=2)
            queryset = self.queryset.filter(schedule__range=(start_filter, end_filter))
        return queryset

    @action(detail=True, methods=['post'])
    def reschedule_activity(self, request, pk=None):
        activity = self.get_object()
        serializer = RescheduleActivitySerializer(data=request.data)
        if serializer.is_valid():
            if activity.status != 'cancelled':
                new_datetime = datetime.strptime(serializer.data['date'], '%Y-%m-%dT%H:%M:%S')
                end_schedule = new_datetime + timedelta(hours=1)
                activities = Activity.objects.filter(property=activity.property, schedule__range=(new_datetime,
                                                                                                  end_schedule))
                if activities:
                    return Response(data={'detail': "Ya existe una actividad en el horario que se proporciono."},
                                    status=status.HTTP_400_BAD_REQUEST)
                else:
                    activity.schedule = new_datetime
                    activity.save()
                    return Response(data=self.serializer_class(activity).data, status=status.HTTP_200_OK)
            else:
                return Response(data={'detail': "No se puede reagendar una actividad cancelada"},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['post'])
    def cancel_activity(self, request, pk=None):
        activity = self.get_object()
        serializer = self.serializer_class(data=request.data)
        if pk:
            try:
                activity.status = 'cancelled'
                activity.save()

                return Response(data=self.serializer_class(activity).data, status=status.HTTP_200_OK)
            except Exception as e:
                return Response(data={'detail': ("Ocurrió el siguiente error" + str(e))},
                                status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)



class PropertyViewSet(viewsets.ModelViewSet):
    queryset = Property.objects.all()
    serializer_class = PropertySerializer
