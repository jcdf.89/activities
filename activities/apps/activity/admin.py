# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from activities.apps.activity.models import Activity, Property, Survey

# Register your models here.
@admin.register(Activity)
class ActivityAdmin(admin.ModelAdmin):
    list_display = ('property', 'schedule', 'title', 'created_at', 'updated_at', 'status')
    list_filter = ('title', 'status')
    search_fields = ['property', 'status']


@admin.register(Property)
class PropertyAdmin(admin.ModelAdmin):
    list_display = ('title', 'address', 'created_at', 'updated_at', 'disabled_at', 'status')
    list_filter = ('title', 'status')
    search_fields = ['property', 'status']


@admin.register(Survey)
class SurveyAdmin(admin.ModelAdmin):
    list_display = ('activity', 'created_at')
    list_filter = ('activity',)
    search_fields = ['activity',]