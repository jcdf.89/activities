import django_filters
from django_filters import rest_framework as filters
from .models import Activity


class ActivityFilter(filters.FilterSet):
    name = django_filters.CharFilter(lookup_expr='icontains')

    class Meta:
        model = Activity
        fields = {'id', 'name', 'status', 'schedule'}
