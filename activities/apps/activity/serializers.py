# -*- coding: utf-8 -*-
from datetime import timedelta

from django.contrib.postgres import fields
from rest_framework import serializers
from activities.apps.activity.models import *


class PropertySerializer(serializers.ModelSerializer):
    """
    Serializer para la creación de propiedades
    """

    class Meta:
        model = Property
        fields = ('id', 'title', 'address')


class ActivitySerializer(serializers.ModelSerializer):
    schedule = serializers.DateTimeField(format="%d-%m-%Y %H:%M:%S")
    """
    Serializer para la creación de actividades
    """

    class Meta:
        model = Activity
        fields = "__all__"

    def to_representation(self, instance):
        representation = super(ActivitySerializer, self).to_representation(instance)
        representation['condition'] = instance.get_condition()
        representation['property'] = PropertySerializer(instance=instance.property).data
        representation['survey'] = self.context['request'].build_absolute_uri('/') + "/api/survey/" + str(instance.id)
        return representation

    def create(self, validated_data):
        disable_properties = Property.objects.filter(status='disable').exists()
        new_datetime = validated_data['schedule']
        end_schedule = new_datetime + timedelta(hours=1)
        same_activities = Activity.objects.filter(schedule__range=(new_datetime,end_schedule),
                                                  property=validated_data['property']).exists()
        try:
            if not disable_properties:
                if not same_activities:
                    activity = Activity.objects.create(**validated_data)
                    return activity
                else:
                    raise serializers.ValidationError("Ya existe una actividad en el mismo horario para la propeidad "
                                                      "seleccionada")
            else:
                raise serializers.ValidationError("Existen propiedades inactivas No se pueden crear actividades si una "
                                                  "Propiedad está desactivada")
        except Exception as e:
            raise serializers.ValidationError("Surgio el siguiente error: " + str(e))

    def update(self, instance, validated_data):
        disable_properties = Property.objects.filter(status='disable').exists()
        same_activities = Activity.objects.filter(schedule__contains=validated_data['schedule'],
                                                  property=validated_data['property']).exists()
        try:
            if not disable_properties:
                if not same_activities:
                    activity = Activity.objects.create(**validated_data)
                    return activity
                else:
                    raise serializers.ValidationError("Ya existe una actividad en el mismo horario para la propeidad "
                                                      "seleccionada")
            else:
                raise serializers.ValidationError("Existen propiedades inactivas No se pueden crear actividades si una "
                                                  "Propiedad está desactivada")
        except Exception as e:
            raise serializers.ValidationError("Surgio el siguiente error: " + str(e))


class RescheduleActivitySerializer(serializers.Serializer):
    """
    Serializador para reagendar actividades
    """
    date = serializers.DateTimeField()